package com.kgsm.mmmtest.repository

import com.kgsm.mmmtest.entity.Product
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository : MongoRepository<Product, String> {}