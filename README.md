QUERY Example

query{
products{
id
}
}

query{
productbyid(
id: "d4d1348f-4654-4b63-a752-b8043ecbe4d6"
){
id
}
}

MUTATION Example

mutation {
newProduct(
name: "Cheese-Burger",
organisation: "KFC",
islive: true,
desc_type: "Breakfast",
desc_livefrom: "2020-10-29T13:34:00.000",
desc_liveto: "2020-10-29T20:34:00.000",
desc_days: "MTW00",
address_street: "LULU",
address_city: "Edapally",
address_pin: "688012",
price_amount: 290,
price_currency: "INR",
price_discount : 20
) {
id
name
organisation
islive
desc_type
desc_livefrom
desc_liveto
desc_days
address_street
address_city
address_pin
price_amount
price_currency
price_discount
}
}

mutation {
deleteProduct(id: )
}

mutation {
updateProduct(
id: "4073cea9-d859-419d-a755-bb5f573cbb81",
islive: false
) {
id
islive
}
}