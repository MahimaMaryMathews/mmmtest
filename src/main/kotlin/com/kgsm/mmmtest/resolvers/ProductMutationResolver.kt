package com.kgsm.mmmtest.resolvers

import com.kgsm.mmmtest.entity.Product
import com.kgsm.mmmtest.repository.ProductRepository
import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import java.util.*

@Component
class ProductMutationResolver (private val productRepository: ProductRepository): GraphQLMutationResolver {
    fun newProduct(
        name: String, 
        organisation: String,
        islive: Boolean,
        desc_type: String,
        desc_livefrom: String,
        desc_liveto: String,
        desc_days: String,
        address_street: String,
        address_city: String,
        address_pin: String,
        price_amount: Float,
        price_currency: String,
        price_discount: Float
        ): Product {
        val product = Product(
            name, 
            organisation,
            islive,
            desc_type,
            desc_livefrom,
            desc_liveto,
            desc_days,
            address_street,
            address_city,
            address_pin,
            price_amount,
            price_currency,
            price_discount
            )
        product.id = UUID.randomUUID().toString()
        productRepository.save(product)
        return product
    }

    fun deleteProduct(id:String): Boolean {
        productRepository.deleteById(id)
        return true
    }

    fun updateProduct(
        id:String, 
        name:String?,
        organisation: String?,
        islive: Boolean?,
        desc_type: String?,
        desc_livefrom: String?,
        desc_liveto: String?,
        desc_days: String?,
        address_street: String?,
        address_city: String?,
        address_pin: String?,
        price_amount:Float?,
        price_currency: String?,
        price_discount: Float?
        ): Product {
        val product = productRepository.findById(id)
        product.ifPresent {
            if(name != null)
            {it.name = name}
            if(organisation != null)
            {it.organisation = organisation}
            if(islive != null)
            {it.islive = islive}
            if(desc_type !=null)
            {it.desc_type = desc_type}
            if(desc_livefrom !=null)
            it.desc_livefrom = desc_livefrom
            if(desc_liveto !=null)
            it.desc_liveto = desc_liveto
            if(desc_days !=null)
            it.desc_days = desc_days
            if(address_street !=null)
            it.address_street = address_street
            if(address_city !=null)
            it.address_city = address_city
            if(address_pin !=null)
            it.address_pin = address_pin
            if(price_amount !=null)
            it.price_amount = price_amount
            if(price_currency !=null)
            it.price_currency = price_currency
            if(price_discount !=null)
            it.price_discount = price_discount
            productRepository.save(it)
        }
        return product.get()
    }
}