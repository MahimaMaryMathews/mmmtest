package com.kgsm.mmmtest.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "Products")
data class Product(
    var name: String,
    var organisation: String,
    var islive: Boolean,
    var desc_type: String,
    var desc_livefrom: String,
    var desc_liveto: String,
    var desc_days: String,
    var address_street: String,
    var address_city: String,
    var address_pin: String,
    var price_amount: Float,
    var price_currency: String,
    var price_discount: Float
) {
    @Id
    var id: String = ""
}