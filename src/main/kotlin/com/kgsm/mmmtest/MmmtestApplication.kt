package com.kgsm.mmmtest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MmmtestApplication

fun main(args: Array<String>) {
	runApplication<MmmtestApplication>(*args)
}
