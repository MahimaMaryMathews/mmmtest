package com.kgsm.mmmtest.resolvers

import com.kgsm.mmmtest.entity.Product
import com.kgsm.mmmtest.repository.ProductRepository
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Component

@Component
class ProductQueryResolver(val productRepository: ProductRepository,
                         private val mongoOperations: MongoOperations) : GraphQLQueryResolver {
    fun products(): List<Product> {
        val list = productRepository.findAll()
        return list
    }

    fun productbyid(id:String): Product {
        val product = productRepository.findById(id)
        return product.get()
    }
}